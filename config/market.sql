-- MySQL dump 10.15  Distrib 10.0.25-MariaDB, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: market
-- ------------------------------------------------------
-- Server version	10.0.25-MariaDB-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Компьютеры',0,1),(2,'Материнские платы',0,1),(3,'Ноутбуки',0,1),(6,'Видео карты',0,1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `products` text NOT NULL,
  `date_order` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'dmitriy','','simonenko.dmitri@gmail.com',1,'{\"5\":6}','2016-08-04 15:19'),(12,'admin','','admin@mail.ru',4,'{\"5\":2,\"9\":1,\"7\":1}','2016-08-09 12:40');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `brand` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `number` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (4,'Asus H110M-R/C/SI',1472,'Asus','Сокет	s-1151\r\nФорм-фактор	micro-ATX\r\nПоддержка процессоров	Intel 6-поколения (s1151): Core i7, i5, i3, Pentium, Celeron\r\nЧипсет	Intel H110\r\nТип памяти	DDR4\r\nКоличество слотов памяти	2 шт.\r\nМакс. частота памяти	2133 МГц\r\nМакс. объем оперативной памяти	32 Гб','352888',2),(5,'Roma Alpha',4589,'','Производитель СPU	AMD\r\nПоколение процессоров Intel	нет\r\nПроцессор (серия)	AMD Аthlon II\r\nКоличество ядер	2 шт.\r\nМодель процессора	AMD Athlon II X2 220, 2,8 ГГц\r\nЧипсет	AMD 760G\r\nТип ОЗУ	DDR3\r\nОбъем ОЗУ	4 Гб\r\nSSD	нет\r\nОбъем HDD	500 Гб\r\nПроизводитель GPU	AMD (ATI)\r\nТип видеокарты	интегрированная\r\nМодель видеокарты	ATI Radeon HD3000\r\nТип оптического привода	нет\r\nТип Blu-Ray	нет\r\nСетевой адаптер	100 Мбит/с\r\nWi-Fi	нет\r\nBluetooth	нет\r\nКард-ридер	нет\r\nОперационная система	Linux\r\nБлок питания	300 Вт\r\nДополнительно	видеокарта встроенная в материнскую плату','327300',1),(6,'GeForce GTX1070',14509,'NVIDIA','Производитель	NVIDIA\r\nСостояние	новая\r\nОбъем памяти	8192 Мб\r\nТип памяти	GDDR-5\r\nШина памяти	256 бит\r\nDirectX	DX-12\r\nЧастота ядра	1797 МГц\r\nЧастота памяти (эффективная)	8108 МГц','367488',6),(7,'Asus X555SJ Black',9061,'Asus','Диагональ экрана	15,6\"\r\nРазрешение экрана	1366x768\r\nПоверхность экрана	матовая\r\nМодель процессора	Intel Pentium N3700, 1,6-2,4 ГГц\r\nКоличество ядер	4 шт.\r\nТип ОЗУ	DDR3\r\nЧастота ОЗУ	1600 МГц\r\nОбъем ОЗУ	4 Гб\r\nОбъем HDD	1 Тб\r\nТип видеокарты	дискретная\r\nМодель видеокарты	NVIDIA GeForce 920M, 1 Гб','322162',3),(8,'Lenovo IdeaPad 100-15 IBY',7099,'Lenovo','Диагональ экрана	15,6\"\r\nРазрешение экрана	1366x768\r\nПоверхность экрана	глянцевая\r\nМодель процессора	Intel Pentium N3540, 2,16-2,66 ГГц\r\nКоличество ядер	4 шт.\r\nТип ОЗУ	DDR3\r\nЧастота ОЗУ	1600 МГц\r\nОбъем ОЗУ	4 Гб','300144',3),(9,'Dell Inspiron 3542 Black',9599,'Dell','Диагональ экрана	15,6\"\r\nРазрешение экрана	1366x768\r\nПоверхность экрана	глянцевая\r\nМодель процессора	Intel Core i3-4005U, 1,7 ГГц\r\nКоличество ядер	2 шт.\r\nТип ОЗУ	DDR3\r\nЧастота ОЗУ	1600 МГц\r\nОбъем ОЗУ	4 Гб','357900',3);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `permission` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'dmitriy','123456','simonenko.dmitri@gmail.com',''),(4,'admin','qwerty','admin@mail.ru','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-09 12:42:33
