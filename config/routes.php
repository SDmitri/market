<?php
return array(


    'user/register' => 'user/register',
    'user/login' => 'user/login',

    'admin/product/create' => 'adminProduct/create',
    'admin/product/update/([0-9]+)' => 'adminProduct/update/$1',
    'admin/product/delete/([0-9]+)' => 'adminProduct/delete/$1',
    'admin/product' => 'adminProduct/index',

    'admin/category/create' => 'adminCategory/create',
    'admin/category/update/([0-9]+)' => 'adminCategory/update/$1',
    'admin/category/delete/([0-9]+)' => 'adminCategory/delete/$1',
    'admin/category' => 'adminCategory/index',

    'admin/order/read/([0-9]+)' => 'adminOrder/read/$1',
    'admin/order/update/([0-9]+)' => 'adminOrder/update/$1',
    'admin/order/delete/([0-9]+)' => 'adminOrder/delete/$1',
    'admin/order' => 'adminOrder/index',

    'product/([0-9]+)' => 'product/view/$1',

    'cabinet/logout' => 'cabinet/logout',
    'cabinet/edit' => 'cabinet/edit',
    'cabinet' => 'cabinet/index',

    'cart/add/([0-9]+)' => 'cart/add/$1',
    'cart/delete/([0-9]+)' => 'cart/delete/$1',
    'cart/checkout' => 'cart/checkout',
    'cart' => 'cart/index',

    'contact' => 'site/contact',

    'category/([0-9]+)' => 'catalog/category/$1',
    'category' => 'catalog/index',

    'catalog/([0-9]+)/page-([0-9]+)' => 'catalog/category/$1/$2',
    'catalog' => 'catalog/index',
    'admin' => 'admin/index',
    '' => 'site/index'
);
