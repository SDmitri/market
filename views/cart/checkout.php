<?php include(ROOT . '/template/layouts/head.php'); ?>
<body>
<div id="main_container">
    <?php //include (ROOT . '/template/layouts/top_bar.php'); ?>
    <?php include(ROOT . '/template/layouts/header.php'); ?>
    <div id="main_content">
        <?php include(ROOT . '/template/layouts/menu.php'); ?>
        <?php include(ROOT . '/template/layouts/categories.php'); ?>
        <div class="center_content">
            <div class="center_title_bar">Корзина</div>
            <div class="prod_box_big">
                <div class="top_prod_box_big"></div>
                <div class="center_prod_box_big">

                    <?php if ($result): ?>
                        <p>Спасибо, Ваш заказ отправлен! С Вами свяжется наш менеджер.</p>
                    <?php else: ?>

                        <div class="contact_form">

                            <?php if (isset($errors) AND is_array($errors)):
                                foreach ($errors as $error): ?>
                                    <li><?php echo $error; ?></li>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <form action="#" method="POST">
                                <div class="form_row">
                                    <input type="text" name="name" placeholder="Имя" value="<?php echo $userName; ?>"/>
                                </div>
                                <div class="form_row">
                                    <input type="email" name="email" placeholder="E-mail"
                                           value="<?php echo $userEmail; ?>"/>
                                </div>
                                <div class="form_row">
                                    <input type="text" name="phone" placeholder="Телефон"/>
                                </div>
                                <div class="form_row">
                                    <input type="submit" name="submit" class="" value="Сделать заказ"/>
                                </div>
                            </form>
                        </div>

                    <?php endif; ?>
                </div>
                <div class="bottom_prod_box_big"></div>
            </div>
        </div>
        <?php include(ROOT . '/template/layouts/shopping_cart.php'); ?>
        <!-- end of right content -->
    </div>
    <!-- end of main content -->
    <?php include(ROOT . '/template/layouts/footer.php'); ?>
</div>
<!-- end of main_container -->
</body>
</html>