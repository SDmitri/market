<?php include(ROOT . '/template/layouts/head.php'); ?>
<body>
<div id="main_container">
    <?php //include (ROOT . '/template/layouts/top_bar.php'); ?>
    <?php include(ROOT . '/template/layouts/header.php'); ?>
    <div id="main_content">
        <?php include(ROOT . '/template/layouts/menu.php'); ?>
        <?php include(ROOT . '/template/layouts/categories.php'); ?>
        <div class="center_content">
            <div class="center_title_bar">Корзина</div>
            <div class="prod_box">
                <div class="top_prod_box_big"></div>
                <div class="center_prod_box_big">
                    <?php if ($cartProducts): ?>
                        <table>
                            <tr>
                                <th>Код товара</th>
                                <th>Название</th>
                                <th>Стоимость, грн</th>
                                <th>Количество, шт</th>
                                <th>Удалить</th>
                            </tr>
                            <?php foreach ($products as $product): ?>
                                <tr>
                                    <td>
                                        <?php echo $product['number']; ?>
                                    </td>
                                    <td>
                                        <?php echo $product['name']; ?>
                                    </td>
                                    <td>
                                        <?php echo $product['price']; ?>
                                    </td>
                                    <td>
                                        <?php echo $cartProducts[$product['id']]; ?>
                                    </td>
                                    <td>
                                        <a href="/cart/delete/<?php echo $product['id'] ?>">X</a>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                            <tr class="totalMoney">
                                <td colspan="4" align="right">
                                    <b>Общая сумма, грн.:</b>
                                </td>
                                <td>
                                    <?php echo $totalMoney; ?>
                                </td>
                            </tr>

                        </table>
                    <?php endif; ?>
                    <a href="/cart/checkout" title="header=[Оформить заказ] body=[&nbsp;] fade=[on]">
                        <img src="/template/images/shoppingcart.png" alt="" width="48" height="48" border="0"/>
                    </a></center>
                </div>
                <div class="bottom_prod_box_big"></div>
                <center>

            </div>
        </div>
        <!-- end of center content -->
        <?php include(ROOT . '/template/layouts/shopping_cart.php'); ?>
        <!-- end of right content -->
    </div>
    <!-- end of main content -->
    <?php include(ROOT . '/template/layouts/footer.php'); ?>
</div>
<!-- end of main_container -->
</body>
</html>