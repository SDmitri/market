<?php include (ROOT . '/template/layouts/head.php'); ?>
<body>
<div id="main_container">
    <?php //include (ROOT . '/template/layouts/top_bar.php'); ?>
    <?php include (ROOT . '/template/layouts/header.php'); ?>
    <div id="main_content">
        <?php include (ROOT . '/template/layouts/menu.php'); ?>
        <?php include (ROOT . '/template/layouts/categories.php'); ?>
        <div class="center_content">
            <div class="center_title_bar">Последние товары</div>
            <?php foreach ($products as $product): ?>
            <div class="prod_box">
                <div class="top_prod_box"></div>
                <div class="center_prod_box">
                    <div class="product_title"><a href="/product/<?php echo $product['id']; ?>"><?php echo $product['name']; ?></a></div>
                    <div class="product_img"><a href="/product/<?php echo $product['id']; ?>"><img src="<?php echo Product::getImage($product['id']); ?>" alt="" border="0" width="100" /></a></div>
                    <div class="prod_price"><span class="price"><?php echo $product['price']; ?> грн.</span></div>
                </div>
                <div class="bottom_prod_box"></div>
                <div class="prod_details_tab">
                    <a href="/cart/add/<?php echo $product['id']; ?>" title="header=[Добавить в корзину] body=[&nbsp;] fade=[on]">
                        <img src="/template/images/cart.gif" alt="" border="0" class="left_bt" />
                    </a>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <!-- end of center content -->
        <?php include(ROOT . '/template/layouts/shopping_cart.php'); ?>
        <!-- end of right content -->
    </div>
    <!-- end of main content -->
<?php include (ROOT . '/template/layouts/footer.php'); ?>
</div>
<!-- end of main_container -->
</body>
</html>