<?php include(ROOT . '/template/layouts/header_admin.php'); ?>
<div class="center_content">
    <div class="center_title_bar">Добавление товара</div>
    <form action="#" method="post" enctype="multipart/form-data">

        <p>Название товара</p>
        <input type="text" name="name" placeholder="" value="" />

        <p>Номер</p>
        <input type="text" name="number" placeholder="" value="" />

        <p>Стоимость, грн.</p>
        <input type="text" name="price" placeholder="" value="" />

        <p>Категория</p>
        <select name="category_id">
            <?php if (is_array($categories)): ?>
                <?php foreach ($categories as $category): ?>
                    <option value="<?php echo $category['id']; ?>">
                        <?php echo $category['name']; ?>
                    </option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>

        <br/><br/>

        <p>Производитель</p>
        <input type="text" name="brand" placeholder="" value="" />

        <p>Изображение товара</p>
        <input type="file" name="image" placeholder="" value="" />

        <p>Описание</p>
        <textarea name="description"></textarea>

        <br/><br/>

        <input type="submit" name="submit" class="btn btn-default" value="Сохранить" />

        <br/><br/>

    </form>
</div>