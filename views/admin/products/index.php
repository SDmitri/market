<?php include(ROOT . '/template/layouts/header_admin.php'); ?>
<div class="center_content">
    <div class="center_title_bar">Список товаров</div>
    <a href="/admin/product/create">Добавить товар</a>
    <br/>
    <br/>
    <table>
        <tr>
            <th>ID</th>
            <th>Номер</th>
            <th>Название</th>
            <th>Цена</th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach ($products as $product): ?>
            <tr>
                <td><?php echo $product['id']; ?></td>
                <td><?php echo $product['number']; ?></td>
                <td><?php echo $product['name']; ?></td>
                <td><?php echo $product['price']; ?></td>
                <td><a href="/admin/product/update/<?php echo $product['id']; ?>">Изменить</a></td>
                <td><a href="/admin/product/delete/<?php echo $product['id']; ?>">Удалить</a></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>