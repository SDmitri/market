<?php include(ROOT . '/template/layouts/header_admin.php'); ?>
<div class="center_content">
    <div class="center_title_bar">Редактирование категории</div>
    <form action="#" method="post">

        <p>Название категории</p>
        <input type="text" name="name" placeholder="" value="<?php echo $category['name']; ?>"/>

        <p>Порядковый номер</p>
        <input type="text" name="sort_order" placeholder="" value="<?php echo $category['sort_order']; ?>"/>
        <br/>
        <br/>
        <select name="status">
            <option value="1">Включена</option>
            <option value="0">Выключена</option>
        </select>
        <br/>
        <br/>
        <input type="submit" name="submit" class="btn btn-default" value="Сохранить"/>

        <br/><br/>
    </form>
</div>