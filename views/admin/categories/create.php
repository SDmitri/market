<?php include(ROOT . '/template/layouts/header_admin.php'); ?>
<div class="center_content">
    <div class="center_title_bar">Добавление категории</div>
    <form action="#" method="post">

        <p>Название категории</p>
        <input type="text" name="name" placeholder="" value="" />

        <p>Порядковый номер</p>
        <input type="text" name="sort_order" placeholder="" value="" />
        <br/>
        <br/>
        <select name="status">
            <option value="1">Включена</option>
            <option value="0">Выключена</option>
        </select>

        <input type="submit" name="submit" class="btn btn-default" value="Сохранить" />

        <br/><br/>

    </form>
</div>