<?php include(ROOT . '/template/layouts/header_admin.php'); ?>
<div class="center_content">
    <div class="center_title_bar">Список категорий</div>
    <a href="/admin/category/create">Добавить категорию</a>
    <br/>
    <br/>
    <table>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Порядковый номер</th>
            <th>Статус</th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach ($categories as $category): ?>
            <tr>
                <td><?php echo $category['id']; ?></td>
                <td><?php echo $category['name']; ?></td>
                <td><?php echo $category['sort_order']; ?></td>
                <?php if ($category['status'] = 1): ?>
                <td>Включена</td>
                <?php else: ?>
                <td>Выключена</td>
                <?php endif; ?>
                <td><a href="/admin/category/update/<?php echo $category['id']; ?>">Изменить</a></td>
                <td><a href="/admin/category/delete/<?php echo $category['id']; ?>">Удалить</a></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>