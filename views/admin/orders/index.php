<?php include(ROOT . '/template/layouts/header_admin.php'); ?>
<div class="center_content">
    <div class="center_title_bar">Список заказов</div>
    <br/>
    <br/>
    <table>
        <tr>
            <th>ID</th>
            <th>Покупатель</th>
            <th>Телефон</th>
            <th>E-mail</th>
            <th>Дата заказа</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach ($orders as $order): ?>
            <tr>
                <td><?php echo $order['id']; ?></td>
                <td><?php echo $order['user']; ?></td>
                <td><?php echo $order['phone']; ?></td>
                <td><?php echo $order['email']; ?></td>
                <td><?php echo $order['date_order']; ?></td>
                <td><a href="/admin/order/read/<?php echo $order['id']; ?>">Подробнее</a></td>
                <td><a href="/admin/order/update/<?php echo $order['id']; ?>">Изменить</a></td>
                <td><a href="/admin/order/delete/<?php echo $order['id']; ?>">Удалить</a></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>