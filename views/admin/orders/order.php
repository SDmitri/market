<?php include(ROOT . '/template/layouts/header_admin.php'); ?>
<div class="center_content">
    <div class="center_title_bar">Заказ #<?php echo $order['id']; ?></div>
    <br/>
    <table>
        <tr>
            <td>Имя покупателя</td>
            <td><?php echo $order['user']; ?></td>
        </tr>
        <tr>
            <td>Телефон покупателя</td>
            <td><?php echo $order['phone']; ?></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td><?php echo $order['email']; ?></td>
        </tr>
        <?php if ($order['user_id'] > 0): ?>
            <tr>
                <td>ID пользователя</td>
                <td><?php echo $order['user_id']; ?></td>
            </tr>
        <?php endif; ?>
        <tr>
            <td><b>Дата заказа</b></td>
            <td><?php echo $order['date_order']; ?></td>
        </tr>
    </table>
    <h5>Список товаров</h5>
    <table border="1px">
        <tr>
            <th>ID</th>
            <th>Номер</th>
            <th>Название</th>
            <th>Цена, грн</th>
            <th>Количество</th>
        </tr>
        <?php foreach ($products as $product): ?>
            <tr>
                <td align="center"><?php echo $product['id']; ?></td>
                <td align="center"><?php echo $product['number']; ?></td>
                <td align="center"><?php echo $product['name']; ?></td>
                <td align="center">$<?php echo $product['price']; ?></td>
                <td align="center"><?php echo $orderProducts[$product['id']]; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <center><a href="/admin/order/">Назад</a></center>
</div>