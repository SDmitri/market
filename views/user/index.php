<?php include(ROOT . '/template/layouts/head.php'); ?>
<body>
<div id="main_container">
    <?php //include (ROOT . '/template/layouts/top_bar.php'); ?>
    <?php include(ROOT . '/template/layouts/header.php'); ?>
    <div id="main_content">
        <?php include(ROOT . '/template/layouts/menu.php'); ?>
        <?php include (ROOT . '/template/layouts/categories.php'); ?>
        <div class="center_content">
            <div class="center_title_bar">Contact Us</div>
            <div class="prod_box_big">
                <div class="top_prod_box_big"></div>
                <div class="center_prod_box_big">
                    <div class="contact_form">
                        <div class="form_row">
                            <label class="contact"><strong>Name:</strong></label>
                            <input type="text" class="contact_input"/>
                        </div>
                        <div class="form_row">
                            <label class="contact"><strong>Email:</strong></label>
                            <input type="text" class="contact_input"/>
                        </div>
                        <div class="form_row">
                            <label class="contact"><strong>Phone:</strong></label>
                            <input type="text" class="contact_input"/>
                        </div>
                        <div class="form_row">
                            <label class="contact"><strong>Company:</strong></label>
                            <input type="text" class="contact_input"/>
                        </div>
                        <div class="form_row">
                            <label class="contact"><strong>Message:</strong></label>
                            <textarea class="contact_textarea"></textarea>
                        </div>
                        <div class="form_row"><a href="#" class="contact">send</a></div>
                    </div>
                </div>
                <div class="bottom_prod_box_big"></div>
            </div>
        </div>
        <?php include(ROOT . '/template/layouts/shopping_cart.php'); ?>
        <!-- end of right content -->
    </div>
    <!-- end of main content -->
    <?php include(ROOT . '/template/layouts/footer.php'); ?>
</div>
<!-- end of main_container -->
</body>
</html>