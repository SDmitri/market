<?php

class AdminOrderController extends Admin
{
    public function actionIndex(){
        self::checkPermission();
        $orders = Order::getOrderList();
        require_once (ROOT . '/views/admin/orders/index.php');
        return true;
    }

    public function actionRead($id){
        self::checkPermission();
        $order = Order::getOrderById($id);
        $orderProducts = json_decode($order['products'], true);
        $productsId = array_keys($orderProducts);
        $products = Product::getProductByIds($productsId);
        require_once (ROOT . '/views/admin/orders/order.php');
        return true;
    }

    public function actionUpdate($id){
        self::checkPermission();
        $order = Order::getOrderById($id);

        if (isset($_POST['submit'])){
            $orderUpdate['user'] = $_POST['user'];
            $orderUpdate['phone'] = $_POST['phone'];
            $orderUpdate['email'] = $_POST['email'];
            Category::updateCategory($id, $orderUpdate);
            header("Location: /admin/order");
        }
        require_once (ROOT . '/views/admin/orders/update.php');
        return true;
    }

    public function actionDelete($id){
        self::checkPermission();
        if (isset($_POST['submit'])){
            Order::deleteOrderById($id);
            header("Location: /admin/order");
        }

        require_once (ROOT . '/views/admin/orders/delete.php');
        return true;
    }

}
