<?php

class AdminCategoryController extends Admin
{
    public function actionIndex(){
        self::checkPermission();
        $categories = Category::getCategoryList();
        require_once (ROOT . '/views/admin/categories/index.php');
        return true;
    }

    public function actionCreate(){
        self::checkPermission();
        $categories = Category::getCategoryList();

        if(isset($_POST['submit'])){
            $category['name'] = $_POST['name'];
            $category['sort_order'] = $_POST['sort_order'];
            $category['status'] = $_POST['status'];
            $errors = false;
            if (empty($category['name'])) {
                $errors[] = 'Не все поля заполнены';
            }
            if (empty($errors)){
                Category::createCategory($category);
                header("Location: /admin/category");

            }
        }
        require_once (ROOT . '/views/admin/categories/create.php');
        return true;
    }

    public function actionUpdate($id){
        self::checkPermission();
        $categories = Category::getCategoryList();
        $category = Category::getCategoryByID($id);

        if (isset($_POST['submit'])){
            $categoryUpdate['name'] = $_POST['name'];
            $categoryUpdate['sort_order'] = $_POST['sort_order'];
            $categoryUpdate['status'] = $_POST['status'];
            $errors = false;
            if (empty($categoryUpdate['name'])) {
                $errors[] = 'Не все поля заполнены';
            }
            if (empty($errors)){
                Category::updateCategory($id, $categoryUpdate);
                header("Location: /admin/category");
            }
        }
        require_once (ROOT . '/views/admin/categories/update.php');
        return true;
    }

    public function actionDelete($id){
        self::checkPermission();
        if (isset($_POST['submit'])){
            Category::deleteCategoryById($id);
            header("Location: /admin/category");
        }
        require_once (ROOT . '/views/admin/categories/delete.php');
        return true;
    }

}
