<?php

class AdminController extends Admin
{
    public function actionIndex(){
        self::checkPermission();
        require_once (ROOT . '/views/admin/index.php');
        return true;

    }

}