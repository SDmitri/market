<?php

class AdminProductController extends Admin
{
    public function actionIndex(){
        self::checkPermission();
        $products = Product::getProductList();
        require_once (ROOT . '/views/admin/products/index.php');
        return true;
    }

    public function actionCreate(){
        self::checkPermission();
        $product = array();
        $categories = Category::getCategoryList();

        if(isset($_POST['submit'])){
            $product['name'] = $_POST['name'];
            $product['number'] = $_POST['number'];
            $product['price'] = $_POST['price'];
            $product['category_id'] = $_POST['category_id'];
            $product['brand'] = $_POST['brand'];
            $product['description'] = $_POST['description'];
            $errors = false;
            if (empty($product['name'])) {
                $errors[] = 'Не все поля заполнены';
            }
            if (empty($errors)){
                $productId = Product::createProduct($product);
                if ($productId){
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"] . "/template/images/products/$productId.png");
                    }
                }
            }

        }
        require_once (ROOT . '/views/admin/products/create.php');
        return true;

    }

    public function actionUpdate($id){
        self::checkPermission();
        $categories = Category::getCategoryList();
        $product = Product::getProductById($id);

        if (isset($_POST['submit'])){
            $productUpdate['name'] = $_POST['name'];
            $productUpdate['number'] = $_POST['number'];
            $productUpdate['price'] = $_POST['price'];
            $productUpdate['category_id'] = $_POST['category_id'];
            $productUpdate['brand'] = $_POST['brand'];
            $productUpdate['description'] = $_POST['description'];
            $errors = false;
            if (empty($productUpdate['name'])) {
                $errors[] = 'Не все поля заполнены';
            }
            if (Product::updateProduct($id, $productUpdate)){
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"] . "/template/images/products/$id.png");
                }
            }
            header("Location: /admin/product");

        }
        require_once (ROOT . '/views/admin/products/update.php');
        return true;
    }

    public function actionDelete($id){
        self::checkPermission();
        if (isset($_POST['submit'])){
            Product::deleteProductById($id);
            header("Location: /admin/product");
        }

        require_once (ROOT . '/views/admin/products/delete.php');
        return true;
    }

}
