<?php

class CabinetController
{
    public function actionIndex(){
        $userId = User::checkLogged();
        require_once (ROOT . '/views/cabinet/index.php');
        return true;
    }

    public function actionEdit(){
        $userId = User::checkLogged();
        $user = User::getUserById($userId);
        $categories = array();
        $categories = Category::getCategoryList();
        $login = $user['login'];
        $password = $user['password'];
        $result = '';

        if (isset($_POST['submit'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];

            $errors = false;
            if (empty(User::checkLogin($login))) {
                $errors[] = 'Не корректное имя';
            }

            if (empty(User::checkPassword($password))) {
                $errors[] = 'Не корректный пароль';
            }

            if ($errors == false){
                $result = User::editUser($userId, $login, $password);
            }
        }

        require_once (ROOT . '/views/cabinet/edit.php');
        return true;
    }

    public function actionHistory(){
        require_once (ROOT . '/views/cabinet/history.php');
        return true;
    }

    public function actionLogout(){
        User::logoutUser();
        return true;
    }

}