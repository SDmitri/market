<?php

class CartController
{
    public function actionAdd($productID){
        Cart::addProduct($productID);
        $url = $_SERVER['HTTP_REFERER'];
        header("Location: $url");
    }

    public function actionDelete($productID){
        Cart::delProduct($productID);
        $url = $_SERVER['HTTP_REFERER'];
        header("Location: /cart");
    }

    public function actionIndex(){
        $categories = array();
        $categories = Category::getCategoryList();
        $cartProducts = false;
        $cartProducts = Cart::getProducts();
        if ($cartProducts){
            $productId = array_keys($_SESSION['products']);
            $products = Product::getProductByIds($productId);
            $totalMoney = Cart::totalMoney($products);
        } else {
            header("Location: /");
        }
        require_once (ROOT . '/views/cart/index.php');
        return true;
    }

    public function actionCheckout(){
        $categories = array();
        $categories = Category::getCategoryList();
        $result = false;
        $errors = false;
        $userId = User::checkUserType();

        if (isset($_POST['submit']) and isset($_SESSION['products'])){
            $userEmail = $_POST['email'];
            $userName = $_POST['name'];
            $userPhone = $_POST['phone'];
            if (empty(User::checkEmail($userEmail))){
                $errors[] = 'Некорректный E-mail';
            }
            if (empty(User::checkLogin($userName))){
                $errors[] = 'Некорректное имя';
            }
            if (empty($errors)){
                $cartProducts = Cart::getProducts();
                $result = Order::create($userName, $userEmail, $userPhone, $userId, $cartProducts);
                if ($result){
                    Cart::clearProducts();
                }
            }

        } else {
            $cartProducts = Cart::getProducts();
            if (empty($cartProducts)){
                header('Location: /');
            } else {
                $productId = array_keys($categories);
                $products = Product::getProductByIds($productId);
                $totalMoney = Cart::totalMoney($products);
                $count = Cart::countProducts();
                $userName = false;
                $userEmail = false;
                $userPhone = false;
                if ($userId){
                    $user = User::getUserById($userId);
                    $userEmail = $user['email'];
                    $userName = $user['login'];
                }
            }
        }
        require_once (ROOT . '/views/cart/checkout.php');
        return true;
    }

}