<?php
class CatalogController
{
    public function actionIndex(){
        $categories = array();
        $categories = Category::getCategoryList();
        $products = array();
        $products = Product::getProductList(3);

        require_once (ROOT . '/views/catalog/index.php');
        return true;
    }

    public function actionCategory($categoryId, $page=1){
        $page = intval($page);
        $categories = array();
        $categories = Category::getCategoryList();
        $categoryProducts = array();
        $categoryProducts = Product::getProductListByCategory($categoryId, $page);
        $productsCount = Product::getCountProducts($categoryId);

        $pagination = new Pagination($productsCount, $page, Product::COUNT_PRODUCT, 'page-');

        require_once (ROOT . '/views/catalog/category.php');
        return true;
    }
}