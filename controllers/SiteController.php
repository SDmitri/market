<?php
class SiteController
{
    public function actionIndex(){
        $categories = array();
        $categories = Category::getCategoryList();
        $products = array();
        $products = Product::getProductList(10);

        require_once (ROOT . '/views/site/index.php');
        return true;
    }
    public function actionContact()
    {
        $categories = array();
        $categories = Category::getCategoryList();
        require_once (ROOT . '/views/site/contact.php');
        return true;
    }
}