<?php
class UserController
{
    public function actionRegister()
    {
        $categories = array();
        $categories = Category::getCategoryList();
        $login = '';
        $email = '';
        $password = '';
        $result = '';
        if (isset($_POST['submit'])) {
            $login = $_POST['login'];
            $email = $_POST['email'];
            $password = $_POST['password'];


            $errors = false;
            if (empty(User::checkLogin($login))) {
                $errors[] = 'Не корректное имя';
            }
            if (empty(User::checkEmail($email))) {
                $errors[] = 'Не корректный E-mail';
            }
            if (empty(User::checkPassword($password))) {
                $errors[] = 'Не корректный пароль';
            }

            if (User::checkEmailExists($email)){
                $errors[] = 'E-mail уже существует';
            }

            if ($errors == false){
                $result = User::registerUser($login, $email, $password);
            }
        }

        require_once (ROOT . '/views/user/register.php');
        return true;
    }

    public function actionLogin()
    {
        $categories = array();
        $categories = Category::getCategoryList();
        $email = '';
        $password = '';
        $result = '';
        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;
            if (empty(User::checkEmail($email))) {
                $errors[] = 'Не корректный E-mail';
            }
            if (empty(User::checkPassword($password))) {
                $errors[] = 'Не корректный пароль';
            }

            $userId = User::loginUser($email, $password);
            if ($userId == false) {
                $errors[] = 'Неправильные E-mail или пароль';
            }else{
                User::authUser($userId);
                header("Location: /cabinet/");
            }
        }

        require_once (ROOT . '/views/user/login.php');
        return true;
    }
}