<?php

class User
{
    public static function loginUser($email, $password){
        $db = Db::getConnection();
        $query = 'SELECT * FROM users WHERE email = :email AND password = :password';
        $result = $db->prepare($query);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->execute();
        $user = $result->fetch();
        if ($user){
            return $user['id'];
        }else{
            return false;
        }
    }

    public static function logoutUser(){
        unset($_SESSION['userId']);
        header ("Location: /");
    }

    public static function authUser($userId){
         $_SESSION['userId'] = $userId;
    }

    public static function checkLogged(){
        if ($_SESSION['userId']){
            return $_SESSION['userId'];
        } else {
            header("Location: /user/login");
        }
    }

    public static function checkUserType(){
        if (isset($_SESSION['userId'])){
            return $_SESSION['userId'];
        } else {
            $userId = 0;
            return $userId;
        }
    }

    public static function getUserById($userId){
        $db = Db::getConnection();
        $query = 'SELECT * FROM users WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $userId, PDO::PARAM_INT);
        $result->execute();
        $user = $result->fetch();
        if ($user){
            return $user;
        }else{
            return false;
        }
    }

    public static function editUser($userId, $login, $password)
    {
        $db = Db::getConnection();
        $query = 'UPDATE users SET login = :login, password = :password WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $userId, PDO::PARAM_INT);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function registerUser($login, $email, $password)
    {
        $db = Db::getConnection();
        $query = 'INSERT INTO users (login , email, password) VALUES (:login, :email, :password)';
        $result = $db->prepare($query);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function checkLogin($login){
        if (strlen($login) > 2){
            return true;
        }else{
            return false;
        }
    }

    public static function checkEmail($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }else{
            return false;
        }
    }

    public static function checkPassword($password){
        if (strlen($password) >= 6){
            return true;
        }else{
            return false;
        }
    }

    public static function checkEmailExists($email){
        $db = Db::getConnection();
        $query = 'SELECT COUNT(*) FROM users WHERE email = :email';
        $result = $db->prepare($query);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        if ($result->fetchColumn()){
            return true;
        }else{
            return false;
        }
    }
}
