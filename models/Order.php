<?php

class Order
{
    public static function create ($userName, $userEmail, $userPhone, $userId, $cartProducts){
        $db = Db::getConnection();
        $cartProducts = json_encode($cartProducts);
        $date = date('Y-m-d H:i');
        $query = 'INSERT INTO orders (user, phone, email, user_id, products, date_order) '
        . 'VALUES (:user, :phone, :email, :user_id, :products, :date_order)';
        $result = $db->prepare($query);
        $result->bindParam(':user', $userName, PDO::PARAM_STR);
        $result->bindParam(':phone', $userPhone, PDO::PARAM_STR);
        $result->bindParam(':email', $userEmail, PDO::PARAM_STR);
        $result->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $result->bindParam(':products', $cartProducts, PDO::PARAM_STR);
        $result->bindParam(':date_order', $date, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function getOrderList(){
        $orders = array();
        $i = 0;
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM orders ORDER BY date_order ASC');

        while ($row = $result->fetch()) {
            $orders[$i]['id'] = $row['id'];
            $orders[$i]['user'] = $row['user'];
            $orders[$i]['phone'] = $row['phone'];
            $orders[$i]['email'] = $row['email'];
            $orders[$i]['user_id'] = $row['user_id'];
            $orders[$i]['products'] = $row['products'];
            $orders[$i]['date_order'] = $row['date_order'];
            $i++;
        }
        return $orders;
    }

    public static function getOrderById($id){
        $order = array();
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM orders WHERE id=' . $id);
        $row = $result->fetch();
        $order['id'] = $row['id'];
        $order['user'] = $row['user'];
        $order['phone'] = $row['phone'];
        $order['email'] = $row['email'];
        $order['user_id'] = $row['user_id'];
        $order['products'] = $row['products'];
        $order['date_order'] = $row['date_order'];
        return $order;
    }

    public static function deleteOrderById ($orderId)
    {
        $db = Db::getConnection();
        $query = 'DELETE FROM orders WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $orderId, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateOrder($id, $order)
    {
        $db = Db::getConnection();
        $query = 'UPDATE order SET user = :user, phone = :phone, email = :email WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':user', $order['user'], PDO::PARAM_STR);
        $result->bindParam(':phone', $order['phone'], PDO::PARAM_STR);
        $result->bindParam(':email', $order['email'], PDO::PARAM_INT);
        return $result->execute();
    }
}