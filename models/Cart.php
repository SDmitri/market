<?php

class Cart
{
    public static function addProduct($productId)
    {
        $productId = intval($productId);
        $cartProducts = array();
        if (isset($_SESSION['products'])) {
            $cartProducts = $_SESSION['products'];
        }
        if (array_key_exists($productId, $cartProducts)) {
            $cartProducts[$productId]++;
        } else {
            $cartProducts[$productId] = 1;
        }
        $_SESSION['products'] = $cartProducts;
        return true;
    }

    public static function delProduct($productId)
    {
        $productId = intval($productId);
        $cartProducts = $_SESSION['products'];
        if (array_key_exists($productId, $cartProducts)) {
            if ($cartProducts[$productId] > 0) {
                $cartProducts[$productId]--;
                if ($cartProducts[$productId] == 0) {
                    unset($cartProducts[$productId]);
                }
            }
        }
        $_SESSION['products'] = $cartProducts;
        return true;
    }

    public static function clearProducts()
    {
        if (isset($_SESSION['products'])) {
            unset($_SESSION['products']);
        }
    }

    public static function countProducts()
    {
        $count = 0;
        if (isset($_SESSION['products'])) {
            foreach ($_SESSION['products'] as $productId => $countProducts) {
                $count = $count + $countProducts;
            }
        }
        return $count;
    }

    public static function getProducts()
    {
        if (isset($_SESSION['products'])) {
            return $_SESSION['products'];
        } else {
            return false;
        }

    }

    public static function totalMoney($products)
    {
        $totalMoney = 0;
        $cartProducts = self::getProducts();
        if ($cartProducts) {
            foreach ($products as $oneProduct) {
                $totalMoney = $totalMoney + $oneProduct['price'] * $cartProducts[$oneProduct['id']];
            }
        }
        return $totalMoney;
    }

}