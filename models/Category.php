<?php

class Category
{
    public static function getCategoryList()
    {
        $categoryList = array();
        $i = 0;
        $db = Db::getConnection();
        $result = $db->query('SELECT id, name, sort_order, status FROM category ORDER BY sort_order ASC');

        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $categoryList[$i]['sort_order'] = $row['sort_order'];
            $categoryList[$i]['status'] = $row['status'];
            $i++;
        }
        return $categoryList;
    }

    public static function getCategoryByID($id)
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id, name, sort_order, status FROM category WHERE id =' . $id);

        $row = $result->fetch();
        $category['id'] = $row['id'];
        $category['name'] = $row['name'];
        $category['sort_order'] = $row['sort_order'];
        $category['status'] = $row['status'];
        return $category;
    }

    public static function deleteCategoryById ($categoryId)
    {
        $db = Db::getConnection();
        $query = 'DELETE FROM category WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $categoryId, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createCategory($category)
    {
        $db = Db::getConnection();
        $query = 'INSERT INTO category (name, sort_order, status) '
            . 'VALUES (:name, :sort_order, :status)';
        $result = $db->prepare($query);
        $result->bindParam(':name', $category['name'], PDO::PARAM_STR);
        $result->bindParam(':sort_order', $category['sort_order'], PDO::PARAM_STR);
        $result->bindParam(':status', $category['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateCategory($id, $category)
    {
        $db = Db::getConnection();
        $query = 'UPDATE category SET name = :name, sort_order = :sort_order, status = :status WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $category['name'], PDO::PARAM_STR);
        $result->bindParam(':sort_order', $category['sort_order'], PDO::PARAM_STR);
        $result->bindParam(':status', $category['status'], PDO::PARAM_INT);
        return $result->execute();
    }
}