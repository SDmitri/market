<?php

class Product
{
    const COUNT_PRODUCT = 10;
    public static function getProductList($count = self::COUNT_PRODUCT)
    {
        $count = intval($count);
        $productList = array();
        $i = 0;
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM product ORDER By id DESC LIMIT ' . $count);

        while ($row = $result->fetch()) {
            $productList[$i]['id'] = $row['id'];
            $productList[$i]['name'] = $row['name'];
            $productList[$i]['price'] = $row['price'];
            $productList[$i]['brand'] = $row['brand'];
            $productList[$i]['description'] = $row['description'];
            $productList[$i]['number'] = $row['number'];
            $productList[$i]['category_id'] = $row['category_id'];
            $i++;
        }
        return $productList;
    }

    public static function getProductListByCategory($categoryId, $pageNumber = 1)
    {
        $count = self::COUNT_PRODUCT;
        $offset = $count * ($pageNumber - 1);
        $categoryId = intval($categoryId);
        $productCategory = array();
        $i = 0;
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM product WHERE category_id =' .$categoryId . ' ORDER By id DESC LIMIT ' . $count . ' OFFSET ' . $offset);

        while ($row = $result->fetch()) {
            $productCategory[$i]['id'] = $row['id'];
            $productCategory[$i]['name'] = $row['name'];
            $productCategory[$i]['price'] = $row['price'];
            $productCategory[$i]['brand'] = $row['brand'];
            $productCategory[$i]['description'] = $row['description'];
            $productCategory[$i]['number'] = $row['number'];
            $productCategory[$i]['category_id'] = $row['category_id'];
            $i++;
        }
        return $productCategory;
    }

    public static function getProductById($productId)
    {
        $product =array();
        $i = 0;
        $db = Db::getConnection();
        $result = $db->query('SELECT * FROM product WHERE id =' .$productId);
        $product = $result->fetch();
        return $product;
    }

    public static function getProductByIds($productId)
    {
        $products =array();
        $i = 0;
        $ids = implode(',', $productId);
        $db = Db::getConnection();
        $query = "SELECT * FROM product WHERE id IN ($ids)";
        $result = $db->query($query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $result->fetch()){
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['number'] = $row['number'];
            $i++;
        }
        return $products;
    }

    public static function getCountProducts ($categoryId)
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT COUNT(*) AS count FROM product WHERE category_id =' . $categoryId);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['count'];
    }

    public static function deleteProductById ($productId)
    {
        $db = Db::getConnection();
        $query = 'DELETE FROM product WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $productId, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getImage($productId)
    {
        $emptyImage = 'empty-product.png';
        $path = '/template/images/products/';
        $pathImage = $path . $productId . '.png';
        if (file_exists($_SERVER['DOCUMENT_ROOT']. $pathImage)) {
            return $pathImage;
        }
        return $path . $emptyImage;
    }

    public static function createProduct($product)
    {
        $db = Db::getConnection();
        $query = 'INSERT INTO product (name, number, price, brand, category_id, description) '
                . 'VALUES (:name, :number, :price, :brand, :category_id, :description)';

        $result = $db->prepare($query);
        $result->bindParam(':name', $product['name'], PDO::PARAM_STR);
        $result->bindParam(':number', $product['number'], PDO::PARAM_STR);
        $result->bindParam(':price', $product['price'], PDO::PARAM_STR);
        $result->bindParam(':brand', $product['brand'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $product['category_id'], PDO::PARAM_INT);
        $result->bindParam(':description', $product['description'], PDO::PARAM_STR);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function updateProduct($id, $product){
        $db = Db::getConnection();
        $query = 'UPDATE product SET name = :name, number = :number, price = :price, brand = :brand, category_id = :category_id, description = :description WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $product['name'], PDO::PARAM_STR);
        $result->bindParam(':number', $product['number'], PDO::PARAM_STR);
        $result->bindParam(':price', $product['price'], PDO::PARAM_STR);
        $result->bindParam(':brand', $product['brand'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $product['category_id'], PDO::PARAM_INT);
        $result->bindParam(':description', $product['description'], PDO::PARAM_STR);
        return $result->execute();
    }
}