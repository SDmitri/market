<?php

class Db
{
    public static function getConnection()
    {
        $pathConfig = ROOT . '/config/db.php';
        $params = include ($pathConfig);
        $dsn = "mysql:host={$params['host']};dbname={$params['database']}";
        $db = new PDO($dsn, $params['username'], $params['password']);
        $db->exec('set names utf8');
        return $db;
    }
}