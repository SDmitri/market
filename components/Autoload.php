<?php
function __autoload ($class_name){
    $class_path = array(
        '/models/',
        '/components/',
    );
    foreach ($class_path as $path){
        $path = ROOT . $path . $class_name . '.php';
        if (is_file($path)){
            include_once ($path);
        }
    }
}