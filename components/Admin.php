<?php

class Admin
{
    public static function checkPermissionForMenu(){
        $userId = User::checkLogged();
        $user = User::getUserById($userId);
        if ($user['permission'] == 'admin'){
            return true;
        } else {
            return false;
        }
    }

    public static function checkPermission(){
        $userId = User::checkLogged();
        $user = User::getUserById($userId);
        if ($user['permission'] == 'admin'){
            return true;
        } else {
            die('Permission denied');
        }
    }
}