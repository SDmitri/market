<div id="menu_tab">
    <div class="left_menu_corner"></div>
    <ul class="menu">
        <li><a href="/" class="nav1">Главная</a></li>
        <li class="divider"></li>
        <li><a href="/catalog/" class="nav2">Каталог продуктов</a></li>
        <li class="divider"></li>
        <li><a href="/cabinet/" class="nav4">Личный кабинет</a></li>
        <li class="divider"></li>
        <li><a href="/contact/" class="nav6">Контакты</a></li>
        <li class="divider"></li>
    </ul>
    <div class="right_menu_corner"></div>
</div>