<div class="left_content">
    <div class="title_box">Categories</div>
    <ul class="left_menu">
        <?php foreach ($categories as $category): ?>
            <li class="<?php if (isset($categoryId) AND $categoryId == $category['id']){ echo 'odd'; }else{ echo 'even'; } ?>">
                <a href="/category/<?php echo $category['id']; ?>"><?php echo $category['name'];?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>