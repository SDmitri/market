<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Electronix Store</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
    <link rel="stylesheet" type="text/css" href="/template/css/style.css"/>
    <!--[if IE 6]>
    <link rel="stylesheet" type="text/css" href="/template/css/iecss.css"/>
    <![endif]-->
    <script type="text/javascript" src="/template/js/windowopen.js"></script>
    <script type="text/javascript" src="/template/js/boxOver.js"></script>
</head>
<body>
<div id="main_container">
<div id="main_content">
    <div id="menu_tab">
        <div class="left_menu_corner"></div>
        <ul class="menu">
            <li><a href="/" class="nav1">Сайт</a></li>
            <li><a href="/admin" class="nav2">Админпанель</a></li>
        </ul>
    </div>
    <div class="left_content">
        <div class="title_box">Действия</div>
        <ul class="left_menu">
            <li class="even">
                <a href="/admin/product">Администрирование продуктов</a>
            </li>
            <li class="even">
                <a href="/admin/category">Администрирование категорий</a>
            </li>
            <li class="even">
                <a href="/admin/order">Администрирование заказов</a>
            </li>
        </ul>
    </div>