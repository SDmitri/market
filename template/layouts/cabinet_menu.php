<div class="left_content">
    <div class="title_box">Меню</div>
    <ul class="left_menu">
        <?php if (Admin::checkPermissionForMenu()): ?>
            <li class="even">
                <a href="/admin">Админпанель
                </a>
            </li>
        <?php endif; ?>
        <li class="even">
            <a href="/cabinet/edit">Мой профиль
            </a>
        </li>
        <li class="even">
            <a href="/cabinet/history">История заказов
            </a>
        </li>
        <li class="even">
            <a href="/cabinet/logout/">Выход
            </a>
        </li>
    </ul>
</div>